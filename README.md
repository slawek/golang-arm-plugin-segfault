# Golang segmentation fault on Alpine ARM64

There is an observed issue with executing the ARM binary on Alpine.
Running a program causes the segmentation fault.
The core dump comprises only three first frames and thousands of goroutines.
The problem occurs if CGO is enabled (default) and the built-in `plugin` package is imported.

## Program

The example program that causes the segmentation fault is provided in the `main.go` file.
It has no logic. It just imports the built-in `plugin` package.

## Build & run

To compile the program, use the `go build` command. It is compiled as the `segfault` executable.

## Results

The problem was observed in Docker containers running in two environments:

- GitLab pipeline managed by the Gitlab Runner installed on virtual machine on ARM architecture
- Raspberry Pi 3

The results were the same in both environments.

Test cases:

- `run-clear` - The executable was compiled using the `golang:1.18-alpine3.15`, `golang:1.20-alpine3.16`, and `golang:1.21-alpine3.17` images
                and run using the `alpine:3.15`, `alpine:3.16` and `alpine:3.17` images.
- `run-build-env` - The executable was compiled and run using the `golang:1.18-alpine3.15`, `golang:1.20-alpine3.16`, and `golang:1.21-alpine3.17` images.
- `run-clear-cgo-disabled` - The executable was compiled using the `golang:1.18-alpine3.15`, `golang:1.20-alpine3.16`, and `golang:1.21-alpine3.17` images
                             with the `CGO_ENABLED=0` environment variable and run using the `alpine:3.15`, `alpine:3.16` and `alpine:3.17` images. 

|                       | `run-clear` | `run-build-env` | `run-clear-cgo-disabled` |
| --------------------- | ----------- | --------------- | ------------------------ |
| Go 1.18,  Alpine 3.15 | Segfault    | Segfault        | No error                 |
| Go 1.20,  Alpine 3.16 | Segfault    | Segfault        | No error                 |
| Go 1.21,  Alpine 3.17 | No error    | No error        | No error                 |

